# CS371p: Object-Oriented Programming Collatz Repo

* Name: Pamela Vazquez De La Cruz

* EID: pv5483

* GitLab ID: pamvazquez1

* HackerRank ID: pamvazquez1

* Git SHA: 5450619f89c99ab7dc77303c37e82bfcc660f7d6

* GitLab Pipelines: https://gitlab.com/pamvazquez1/cs371p-collatz/-/pipelines

* Estimated completion time: 20 hours

* Actual completion time: 28

* Comments: Have never programmed in C++ so this should be interesting!
