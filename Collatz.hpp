// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ------------
// cycle_length
// ------------
/**
 * @param a long
 * @return an int
 */

int cycle_length (long n);

// ------------
// max_cy_len
// ------------

/**
 * @param a pair of ints
 * @return an int
 */
int max_cy_len(int i, int j);

// ------------
// mcl_cache
// ------------

/**
 * @param a pair of ints
 * @return an int
 */
int mcl_cache(int i, int j);

// ------------
// optimize_mcl
// ------------
/**
 * @param a pair of ints
 * @return an int
 */
int optimize_mcl(int i, int j);

// ------------
// collatz_eval
// ------------


// ------------
// pre_compute
// ------------
void precompute();

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);



// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
