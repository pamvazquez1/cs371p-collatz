// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1111, 2450)), make_tuple(1111, 2450, 183));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1500, 3500)), make_tuple(1500, 3500, 217));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(1500, 5500)), make_tuple(1500, 5500, 238));
}

//Checks for wrong order input --> passes
TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 500)), make_tuple(1000, 500, 179));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(1, 500)), make_tuple(1, 500, 144));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(112000, 113000)), make_tuple(112000, 113000, 323));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1000)), make_tuple(1, 1000, 179));
}

// -----
// mcl cache
// -----

TEST(CollatzFixture, mcl_cache10) {
    ASSERT_EQ(mcl_cache(1000, 2000), 182);
}

TEST(CollatzFixture, mcl_cache11) {
    ASSERT_EQ(mcl_cache(1000, 4000), 238);
}

// -----
// optimize_mcl
// -----

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(optimize_mcl(100, 1000), 179);
}


TEST(CollatzFixture, eval13) {
    ASSERT_EQ(optimize_mcl(1000, 100), 179);
}


// -----
// max_cy_len
// -----

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(max_cy_len(8, 8), 4);
}

TEST(CollatzFixture, eval15) {
    ASSERT_EQ(max_cy_len(250, 1500), 182);
}

TEST(CollatzFixture, eval16) {
    ASSERT_EQ(max_cy_len(691665, 751172), 504);
}

// -----
// cycle_length
// -----

TEST(CollatzFixture, eval17) {
    ASSERT_EQ(cycle_length (22), 16);
}

TEST(CollatzFixture, eval18) {
    ASSERT_EQ(cycle_length (8), 4);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
